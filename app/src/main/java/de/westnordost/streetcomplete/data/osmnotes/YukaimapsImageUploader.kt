package de.westnordost.streetcomplete.data.osmnotes

import de.westnordost.streetcomplete.ApplicationConstants
import de.westnordost.streetcomplete.data.download.ConnectionException
import de.westnordost.streetcomplete.data.osmnotes.StreetCompleteImageUploader
import de.westnordost.streetcomplete.data.user.OIDCStore
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLConnection
//import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.MediaType
//import okhttp3.internal.Version
import android.webkit.MimeTypeMap

/** Upload and activate a list of image paths to an instance of the Yukaimaps backend
 */
class YukaimapsImageUploader(private val baseUrl: String, private val oidcStore: OIDCStore) {

    /** Upload list of images.
     *
     *  @throws ImageUploadServerException when there was a server error on upload (server error)
     *  @throws ImageUploadClientException when the server rejected the upload request (client error)
     *  @throws ConnectionException if it is currently not reachable (no internet etc) */
    fun upload(imagePaths: List<String>): List<String> {
        val imageLinks = ArrayList<String>()

        for (path in imagePaths) {
            val file = File(path)
            if (!file.exists()) continue
            val contentType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)

            try {
                val requestBuilder = createRequest("/upload")

                val fileBody = RequestBody.create(
                    MediaType.parse(contentType),
                    file.readBytes()
                )
                val body = MultipartBody.Builder().setType(
                    MultipartBody.FORM
                ).addFormDataPart(
                    "fileb", file.getName(), fileBody
                ).build()

                val request = requestBuilder.post(body).build()

                val client = OkHttpClient()
                    .newBuilder()
                    .build()

                val response = client.newCall(request).execute();

                if (response.isSuccessful()) {
                    val content = response.body()?.string()
                    try {
                        val jsonResponse = JSONObject(content)
                        val relativeUurl = jsonResponse.getString("relative_url")
                        imageLinks.add(baseUrl + relativeUurl)
                    } catch (e: JSONException) {
                        throw ImageUploadServerException("Upload Failed: Unexpected response \"$content\"")
                    }
                } else {
                    val status = response.code()
                    val error = response.message()
                    if (status / 100 == 5) {
                        throw ImageUploadServerException("Upload failed: Error code $status, Message: \"$error\"")
                    } else {
                        throw ImageUploadClientException("Upload failed: Error code $status, Message: \"$error\"")
                    }
                }
            } catch (e: IOException) {
                throw ConnectionException("Upload failed", e)
            }
        }

        return imageLinks
    }

    /** Activate the images in the given note.
     *  @throws ImageUploadServerException when there was a server error on upload (server error)
     *  @throws ImageUploadClientException when the server rejected the upload request (client error)
     *  @throws ConnectionException if it is currently not reachable (no internet etc)  */
    fun activate(noteId: Long) {
        // we don't really need to activate photos uploads as only trsuted users
        // will upload.
        // But we keep this hooks to create a future link between photos and notes
        // for housekeeping on closed notes.
    }

    private fun createRequest(url: String): Request.Builder {
        val request = Request.Builder().url(baseUrl + url).addHeader(
            "User-Agent", ApplicationConstants.USER_AGENT
        ).addHeader(
            "Authorization", "Bearer " + oidcStore.token
        )
        return request
    }
}

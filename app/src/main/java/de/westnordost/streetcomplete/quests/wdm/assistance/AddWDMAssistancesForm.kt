package de.westnordost.streetcomplete.quests.wdm.assistance

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMAssistancesForm :
    AImageListQuestForm<List<WDMAssistance>, WDMAssistancesAnswer>() {

    override val descriptionResId = R.string.quest_wdm_assistances_note

    override val items get() = WDMAssistance.selectableValues.map { it.asItem() }

    override val maxSelectableItems = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMAssistance>>) {
        applyAnswer(WDMAssistances(selectedItems.flatten()))
    }
}

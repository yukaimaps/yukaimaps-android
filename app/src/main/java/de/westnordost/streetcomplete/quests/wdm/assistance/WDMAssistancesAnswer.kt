package de.westnordost.streetcomplete.quests.wdm.assistance

sealed interface WDMAssistancesAnswer

data class WDMAssistances(val assistances: List<WDMAssistance>) : WDMAssistancesAnswer

package de.westnordost.streetcomplete.quests.wdm.audible_signals

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMAudibleSignals : OsmFilterQuestType<WDMAudibleSignals>() {

    override val elementFilter = """
        nodes, ways with Quay
        and !AudibleSignals
    """
    override val changesetComment = "WDM Survey audible signals"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_audible_signals
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_audible_signals_title

    override fun createForm() = AddWDMAudibleSignalsForm()

    override fun applyAnswerTo(answer: WDMAudibleSignals, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["AudibleSignals"] = answer.osmValue
    }
}

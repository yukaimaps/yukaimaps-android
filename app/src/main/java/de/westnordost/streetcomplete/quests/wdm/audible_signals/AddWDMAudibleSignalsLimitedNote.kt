package de.westnordost.streetcomplete.quests.wdm.audible_signals

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMAudibleSignalsLimitedNote : OsmFilterQuestType<String>() {

    override val elementFilter = """
        nodes, ways with Quay
        and AudibleSignals = limited
        and !AudibleSignals:Description
    """
    override val changesetComment = "WDM Survey limited note audible signals"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_audible_signals
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_audible_signals_limited_note_title

    override fun createForm() = AddWDMAudibleSignalsLimitedNoteForm()

    override fun applyAnswerTo(answer: String, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["AudibleSignals:Description"] = answer
    }
}

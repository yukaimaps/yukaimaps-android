package de.westnordost.streetcomplete.quests.wdm.audio_information

enum class WDMAudioInformation(val osmValue: String) {
    ADAPTED("adapted"),
    UNADAPTED("unadapted"),
    NO("no"),
}

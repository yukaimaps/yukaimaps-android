package de.westnordost.streetcomplete.quests.wdm.baby_change

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMBabyChangeForm :
    AImageListQuestForm<List<WDMBabyChange>, WDMBabyChangesAnswer>() {

    override val descriptionResId = R.string.quest_wdm_baby_change_note

    override val items get() = WDMBabyChange.selectableValues.map { it.asItem() }

    override val maxSelectableItems = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMBabyChange>>) {
        applyAnswer(WDMBabyChanges(selectedItems.flatten()))
    }
}

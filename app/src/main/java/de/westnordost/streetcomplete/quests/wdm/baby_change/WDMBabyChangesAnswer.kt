package de.westnordost.streetcomplete.quests.wdm.baby_change

sealed interface WDMBabyChangesAnswer

data class WDMBabyChanges(val babyChanges: List<WDMBabyChange>) : WDMBabyChangesAnswer

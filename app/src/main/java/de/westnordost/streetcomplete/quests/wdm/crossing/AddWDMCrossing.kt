package de.westnordost.streetcomplete.quests.wdm.crossing

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.RAIL
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.URBANRAIL
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.ROAD
import de.westnordost.streetcomplete.quests.wdm.crossing.WDMCrossing.CYCLEWAY

class AddWDMCrossing : OsmFilterQuestType<WDMCrossing>() {

    override val elementFilter = "ways with SitePathLink and SitePathLink = Crossing and !Crossing"
    override val changesetComment = "Specify type of crossing"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_crossing
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_crossing_title

    override fun createForm() = AddWDMCrossingForm()

    override fun applyAnswerTo(answer: WDMCrossing, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            RAIL -> {
                tags["Crossing"] = "Rail"
            }
            URBANRAIL -> {
                tags["Crossing"] = "UrbanRail"
            }
            ROAD -> {
                tags["Crossing"] = "Road"
            }
            CYCLEWAY -> {
                tags["Crossing"] = "CycleWay"
            }
        }
    }
}

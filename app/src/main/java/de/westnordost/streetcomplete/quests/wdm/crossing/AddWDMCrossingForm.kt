package de.westnordost.streetcomplete.quests.wdm.crossing

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMCrossingForm : AImageListQuestForm<WDMCrossing, WDMCrossing>() {

    override val items = WDMCrossing.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMCrossing>) {
        applyAnswer(selectedItems.single())
    }
}

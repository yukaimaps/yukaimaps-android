package de.westnordost.streetcomplete.quests.wdm.crossing

enum class WDMCrossing {
    RAIL,
    URBANRAIL,
    ROAD,
    CYCLEWAY
}

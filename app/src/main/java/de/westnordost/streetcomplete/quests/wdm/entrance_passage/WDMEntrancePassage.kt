package de.westnordost.streetcomplete.quests.wdm.entrance_passage

enum class WDMEntrancePassage {
    OPENING,
    DOOR,
    GATE,
    BARRIER
}

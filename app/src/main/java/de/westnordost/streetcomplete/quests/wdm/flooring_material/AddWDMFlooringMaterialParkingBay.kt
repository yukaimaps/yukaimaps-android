package de.westnordost.streetcomplete.quests.wdm.flooring_material

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.ASPHALT
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CONCRETE
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.WOOD
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.EARTH
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.GRAVEL
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.PAVINGSTONES
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.GRASS
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CERAMICTILES
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.PLASTICMATTING
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.STEELPLATE
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.SAND
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.STONE
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CARPET
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.RUBBER
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.CORK
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.FIBREGLASSGRATING
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.GLAZEDCERAMICTILES
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.VINYL
import de.westnordost.streetcomplete.quests.wdm.flooring_material.WDMFlooringMaterial.UNEVEN

class AddWDMFlooringMaterialParkingBay : OsmFilterQuestType<WDMFlooringMaterial>() {

    override val elementFilter = "nodes, ways with ParkingBay = Disabled and !FlooringMaterial"
    override val changesetComment = "Specify type of flooring material of parking bay"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_flooring_parking_bay
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_flooring_material_parkingBay_title

    override fun createForm() = AddWDMFlooringMaterialParkingBayForm()

    override fun applyAnswerTo(answer: WDMFlooringMaterial, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            ASPHALT -> {
                tags["FlooringMaterial"] = "Asphalt"
            }
            CONCRETE -> {
                tags["FlooringMaterial"] = "Concrete"
            }
            WOOD -> {
                tags["FlooringMaterial"] = "Wood"
            }
            EARTH -> {
                tags["FlooringMaterial"] = "Earth"
            }
            GRAVEL -> {
                tags["FlooringMaterial"] = "Gravel"
            }
            PAVINGSTONES -> {
                tags["FlooringMaterial"] = "PavingStones"
            }
            GRASS -> {
                tags["FlooringMaterial"] = "Grass"
            }
            CERAMICTILES -> {
                tags["FlooringMaterial"] = "CeramicTiles"
            }
            PLASTICMATTING -> {
                tags["FlooringMaterial"] = "PlasticMatting"
            }
            STEELPLATE -> {
                tags["FlooringMaterial"] = "SteelPlate"
            }
            SAND -> {
                tags["FlooringMaterial"] = "Sand"
            }
            STONE -> {
                tags["FlooringMaterial"] = "Stone"
            }
            CARPET -> {
                tags["FlooringMaterial"] = "Carpet"
            }
            RUBBER -> {
                tags["FlooringMaterial"] = "Rubber"
            }
            CORK -> {
                tags["FlooringMaterial"] = "Cork"
            }
            FIBREGLASSGRATING -> {
                tags["FlooringMaterial"] = "FibreglassGrating"
            }
            GLAZEDCERAMICTILES -> {
                tags["FlooringMaterial"] = "GlazedCeramicTiles"
            }
            VINYL -> {
                tags["FlooringMaterial"] = "Vinyl"
            }
            UNEVEN -> {
                tags["FlooringMaterial"] = "Uneven"
            }
        }
    }
}

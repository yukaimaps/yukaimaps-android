package de.westnordost.streetcomplete.quests.wdm.flooring_material

enum class WDMFlooringMaterial {
    ASPHALT,
    CONCRETE,
    WOOD,
    EARTH,
    GRAVEL,
    PAVINGSTONES,
    GRASS,
    CERAMICTILES,
    PLASTICMATTING,
    STEELPLATE,
    SAND,
    STONE,
    CARPET,
    RUBBER,
    CORK,
    FIBREGLASSGRATING,
    GLAZEDCERAMICTILES,
    VINYL,
    UNEVEN
}

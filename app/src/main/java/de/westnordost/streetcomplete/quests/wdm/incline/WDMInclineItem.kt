package de.westnordost.streetcomplete.quests.wdm.incline

import android.content.Context
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.incline_direction.Incline
import de.westnordost.streetcomplete.quests.wdm.incline.WDMIncline.UP
import de.westnordost.streetcomplete.quests.wdm.incline.WDMIncline.DOWN
//import de.westnordost.streetcomplete.quests.wdm.incline.WDMIncline.NO
import de.westnordost.streetcomplete.view.DrawableImage
import de.westnordost.streetcomplete.view.ResText
import de.westnordost.streetcomplete.view.RotatedCircleDrawable
import de.westnordost.streetcomplete.view.image_select.DisplayItem
import de.westnordost.streetcomplete.view.image_select.Item
import de.westnordost.streetcomplete.view.image_select.Item2

/*fun WDMIncline.asItem(): Item<List<WDMIncline>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMIncline.iconResId: Int get() = when (this) {
    UP ->     R.drawable.empty
    DOWN ->             R.drawable.empty
    NO ->             R.drawable.empty
}

private val WDMIncline.titleResId: Int get() = when (this) {
    UP ->     R.string.quest_wdm_incline_up
    DOWN ->             R.string.quest_wdm_incline_down
    NO ->             R.string.quest_wdm_incline_no
}*/

fun WDMIncline.asItem(context: Context, rotation: Float): DisplayItem<WDMIncline> {
    val drawable = RotatedCircleDrawable(context.getDrawable(iconResId)!!)
    drawable.rotation = rotation
    return Item2(this, DrawableImage(drawable), ResText(R.string.quest_steps_incline_up))
}

private val WDMIncline.iconResId: Int get() = when (this) {
    WDMIncline.UP -> R.drawable.ic_steps_incline_up
    WDMIncline.DOWN -> R.drawable.ic_steps_incline_up_reversed
}

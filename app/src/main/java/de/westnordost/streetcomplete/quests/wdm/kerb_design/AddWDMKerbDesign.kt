package de.westnordost.streetcomplete.quests.wdm.kerb_design

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Node
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.BICYCLIST
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.BLIND
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.kerb.couldBeAKerb
import de.westnordost.streetcomplete.osm.kerb.findAllKerbNodes
import de.westnordost.streetcomplete.osm.updateWithCheckDate
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip

class AddWDMKerbDesign : OsmFilterQuestType<WDMKerbDesign>() {

    override val elementFilter = "nodes with PathJunction and PathJunction = Crossing and !KerbDesign"
    override val changesetComment = "Determine the design of kerbs"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_kerb_type
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_kerb_design_title

    override fun createForm() = AddWDMKerbDesignForm()

    override fun applyAnswerTo(answer: WDMKerbDesign, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            WDMKerbDesign.NONE -> {
                tags["KerbDesign"] = "None"
            }
            WDMKerbDesign.RAISED -> {
                tags["KerbDesign"] = "Raised"
            }
            WDMKerbDesign.LOWERED -> {
                tags["KerbDesign"] = "Lowered"
            }
            WDMKerbDesign.FLUSHED -> {
                tags["KerbDesign"] = "Flushed"
            }
            WDMKerbDesign.KERBCUT -> {
                tags["KerbDesign"] = "KerbCut"
            }
        }
    }
}

package de.westnordost.streetcomplete.quests.wdm.kerb_design

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.kerb_design.WDMKerbDesign.NONE
import de.westnordost.streetcomplete.quests.wdm.kerb_design.WDMKerbDesign.RAISED
import de.westnordost.streetcomplete.quests.wdm.kerb_design.WDMKerbDesign.LOWERED
import de.westnordost.streetcomplete.quests.wdm.kerb_design.WDMKerbDesign.FLUSHED
import de.westnordost.streetcomplete.quests.wdm.kerb_design.WDMKerbDesign.KERBCUT
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMKerbDesign.asItem() = Item(this, iconResId, titleResId)

private val WDMKerbDesign.titleResId: Int get() = when (this) {
    NONE ->   R.string.quest_wdm_kerb_design_none
    RAISED ->    R.string.quest_wdm_kerb_design_raised
    LOWERED ->   R.string.quest_wdm_kerb_design_lowered
    FLUSHED ->     R.string.quest_wdm_kerb_design_flushed
    KERBCUT -> R.string.quest_wdm_kerb_design_kerb_cut
}

private val WDMKerbDesign.iconResId: Int get() = when (this) {
    NONE ->   R.drawable.kerb_height_no
    RAISED ->    R.drawable.kerb_height_raised
    LOWERED ->   R.drawable.kerb_height_lowered
    FLUSHED ->     R.drawable.kerb_height_flush
    KERBCUT -> R.drawable.kerb_height_lowered_ramp
}

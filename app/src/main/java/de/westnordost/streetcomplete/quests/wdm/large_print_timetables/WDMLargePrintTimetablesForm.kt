package de.westnordost.streetcomplete.quests.wdm.large_print_timetables

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem

class WDMLargePrintTimetablesForm : AbstractOsmQuestForm<Boolean>() {

    override val contentLayoutResId = R.layout.quest_wdm_large_print_timetables

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(false) },
        AnswerItem(R.string.quest_generic_hasFeature_yes) { applyAnswer(true) }
    )
}

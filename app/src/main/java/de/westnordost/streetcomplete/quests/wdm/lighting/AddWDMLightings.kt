package de.westnordost.streetcomplete.quests.wdm.lighting

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMLightings : OsmElementQuestType<WDMLightingsAnswer> {

    private val filter by lazy { """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator
        and !Lighting
    """.toElementFilterExpression() }

    override val changesetComment = "Specify what kind of lighting"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_lighting
    override val isDeleteElementEnabled = true
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_lighting_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.nodes.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        /* Only recycling containers that do either not have any recycling:* tag yet or
         * haven't been touched for 2 years and are exclusively recycling types selectable in
         * StreetComplete. */
        filter.matches(element)

    override fun createForm() = AddWDMLightingsForm()

    override fun applyAnswerTo(answer: WDMLightingsAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        if (answer is WDMLightings) {
            tags["Lighting"] = answer.lightings[0].value.toString()
        }
    }
}

package de.westnordost.streetcomplete.quests.wdm.lighting

import android.os.Bundle
import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMLightingsForm :
    AImageListQuestForm<List<WDMLighting>, WDMLightingsAnswer>() {

    override val descriptionResId = R.string.quest_wdm_lighting_note

    override val items get() = WDMLighting.selectableValues.map { it.asItem() }

    override val maxSelectableItems = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageSelector.cellLayoutId = R.layout.cell_icon_select_with_label_below
    }

    override fun onClickOk(selectedItems: List<List<WDMLighting>>) {
        applyAnswer(WDMLightings(selectedItems.flatten()))
    }
}

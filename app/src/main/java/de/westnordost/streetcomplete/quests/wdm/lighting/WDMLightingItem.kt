package de.westnordost.streetcomplete.quests.wdm.lighting

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.lighting.WDMLighting.YES
import de.westnordost.streetcomplete.quests.wdm.lighting.WDMLighting.NO
import de.westnordost.streetcomplete.quests.wdm.lighting.WDMLighting.BAD
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMLighting.asItem(): Item<List<WDMLighting>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMLighting.iconResId: Int get() = when (this) {
    YES ->     R.drawable.wdm_lighting_yes
    NO ->             R.drawable.wdm_lighting_no
    BAD ->             R.drawable.wdm_lighting_bad
}

private val WDMLighting.titleResId: Int get() = when (this) {
    YES ->     R.string.quest_wdm_lighting_yes
    NO ->             R.string.quest_wdm_lighting_no
    BAD ->             R.string.quest_wdm_lighting_bad
}

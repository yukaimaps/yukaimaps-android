package de.westnordost.streetcomplete.quests.wdm.lighting

sealed interface WDMLightingsAnswer

data class WDMLightings(val lightings: List<WDMLighting>) : WDMLightingsAnswer

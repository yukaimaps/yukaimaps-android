package de.westnordost.streetcomplete.quests.wdm.outdoor

/** All Outdoor:* keys known to StreetComplete */
enum class WDMOutdoor(val value: String) {
    YES("Yes"),
    NO ("No"),
    COVERED("Covered");

    companion object {
        val selectableValues = listOf(
            YES, NO, COVERED
        )
    }
}

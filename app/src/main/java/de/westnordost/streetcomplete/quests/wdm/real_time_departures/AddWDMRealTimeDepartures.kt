package de.westnordost.streetcomplete.quests.wdm.real_time_departures

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.updateWithCheckDate
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMRealTimeDepartures : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
        and !RealTimeDepartures
    """

    override val changesetComment = "WDM Survey real time departures"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_real_time_departures
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_real_time_departures_title

    override fun createForm() = WDMRealTimeDeparturesForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags.updateWithCheckDate("RealTimeDepartures", answer.toYesNo())
    }
}

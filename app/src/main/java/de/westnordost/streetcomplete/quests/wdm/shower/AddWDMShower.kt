package de.westnordost.streetcomplete.quests.wdm.shower

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.quest.AllCountriesExcept
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.YesNoQuestForm
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMShower : OsmFilterQuestType<Boolean>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest = StopPlace
        and Toilets ~ Yes|Bad
        and !Toilets:Shower
    """

    override val changesetComment = "Specify erp toilets shower"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_shower
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_shower_title

    override fun createForm() = YesNoQuestForm()

    override fun applyAnswerTo(answer: Boolean, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["Toilets:Shower"] = answer.toYesNo()
    }
}

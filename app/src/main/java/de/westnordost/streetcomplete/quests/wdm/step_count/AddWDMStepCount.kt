package de.westnordost.streetcomplete.quests.wdm.step_count

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMStepCount : OsmFilterQuestType<Int>() {

    override val elementFilter = """
        ways with SitePathLink = Stairs
         and !StepCount
    """

    override val changesetComment = "WDM Specify step counts"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_steps_count
    // because the user needs to start counting at the start of the steps
    override val hasMarkersAtEnds = true
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_step_count_title

    override fun createForm() = AddWDMStepCountForm()

    override fun applyAnswerTo(answer: Int, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["StepCount"] = answer.toString()
    }
}

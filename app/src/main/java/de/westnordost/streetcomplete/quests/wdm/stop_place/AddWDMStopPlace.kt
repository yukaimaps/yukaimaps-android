package de.westnordost.streetcomplete.quests.wdm.stop_place

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.RAILSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.METROSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.BUSSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.COACHSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.TRAMSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.FERRYPORT
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.LIFTSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.AIRPORT

class AddWDMStopPlace : OsmFilterQuestType<WDMStopPlace>() {

    override val elementFilter = "nodes, ways with PointOfInterest = StopPlace and !StopPlace"
    override val changesetComment = "Specify type of stop place"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_default_poi
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_stop_place_title

    override fun createForm() = AddWDMStopPlaceForm()

    override fun applyAnswerTo(answer: WDMStopPlace, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            RAILSTATION -> {
                tags["StopPlace"] = "RailStation"
            }
            METROSTATION -> {
                tags["StopPlace"] = "MetroStation"
            }
            BUSSTATION -> {
                tags["StopPlace"] = "BusStation"
            }
            COACHSTATION -> {
                tags["StopPlace"] = "CoachStation"
            }
            TRAMSTATION -> {
                tags["StopPlace"] = "TramStation"
            }
            FERRYPORT -> {
                tags["StopPlace"] = "FerryPort"
            }
            LIFTSTATION -> {
                tags["StopPlace"] = "LiftStation"
            }
            AIRPORT -> {
                tags["StopPlace"] = "Airport"
            }
        }
    }
}

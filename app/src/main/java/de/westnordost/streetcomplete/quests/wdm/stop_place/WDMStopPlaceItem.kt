package de.westnordost.streetcomplete.quests.wdm.stop_place

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.RAILSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.METROSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.BUSSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.COACHSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.FERRYPORT
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.LIFTSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.TRAMSTATION
import de.westnordost.streetcomplete.quests.wdm.stop_place.WDMStopPlace.AIRPORT
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMStopPlace.asItem() = Item(this, iconResId, titleResId)

private val WDMStopPlace.titleResId: Int get() = when (this) {
    RAILSTATION ->  R.string.quest_wdm_stop_place_rail_station
    METROSTATION -> R.string.quest_wdm_stop_place_metro_station
    BUSSTATION ->      R.string.quest_wdm_stop_place_bus_station
    COACHSTATION ->      R.string.quest_wdm_stop_place_coach_station
    TRAMSTATION ->      R.string.quest_wdm_stop_place_tram_station
    FERRYPORT ->      R.string.quest_wdm_stop_place_ferry_port
    LIFTSTATION ->      R.string.quest_wdm_stop_place_lift_station
    AIRPORT ->      R.string.quest_wdm_stop_place_airport
}

private val WDMStopPlace.iconResId: Int get() = when (this) {
    RAILSTATION ->  R.drawable.wdm_default_photo
    METROSTATION -> R.drawable.wdm_stop_place_metro_station
    BUSSTATION ->      R.drawable.wdm_stop_place_bus_station
    COACHSTATION ->      R.drawable.wdm_stop_place_coach_station
    TRAMSTATION ->      R.drawable.wdm_stop_place_tram_station
    FERRYPORT ->      R.drawable.wdm_stop_place_ferry_port
    LIFTSTATION ->      R.drawable.wdm_stop_place_lift_station
    AIRPORT ->      R.drawable.wdm_default_photo
}

package de.westnordost.streetcomplete.quests.wdm.structure_type

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMStructureTypes : OsmElementQuestType<WDMStructureTypesAnswer> {

    private val filter by lazy { """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator
        and !StructureType
    """.toElementFilterExpression() }

    override val changesetComment = "Specify what kind of structure type"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_structure_types
    override val isDeleteElementEnabled = true
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_structure_type_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.nodes.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        /* Only recycling containers that do either not have any recycling:* tag yet or
         * haven't been touched for 2 years and are exclusively recycling types selectable in
         * StreetComplete. */
        filter.matches(element)

    override fun createForm() = AddWDMStructureTypesForm()

    override fun applyAnswerTo(answer: WDMStructureTypesAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        if (answer is WDMStructureTypes) {
            tags["StructureType"] = answer.structureTypes[0].value.toString()
        }
    }
}

package de.westnordost.streetcomplete.quests.wdm.structure_type

/** All StructureType:* keys known to StreetComplete */
enum class WDMStructureType(val value: String) {
    PATHWAY("Pathway"),
    OVERPASS ("Overpass"),
    UNDERPASS("Underpass"),
    TUNNEL("Tunnel"),
    CORRIDOR("Corridor");

    companion object {
        val selectableValues = listOf(
            PATHWAY, OVERPASS, UNDERPASS, TUNNEL, CORRIDOR
        )
    }
}

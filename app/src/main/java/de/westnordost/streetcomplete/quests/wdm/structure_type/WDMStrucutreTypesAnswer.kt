package de.westnordost.streetcomplete.quests.wdm.structure_type

sealed interface WDMStructureTypesAnswer

data class WDMStructureTypes(val structureTypes: List<WDMStructureType>) : WDMStructureTypesAnswer

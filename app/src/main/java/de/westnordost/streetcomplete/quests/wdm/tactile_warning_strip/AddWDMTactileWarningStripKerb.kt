package de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.CITIZEN
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.NONE
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.GOOD
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.WORN
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.HAZARDOUS

class AddWDMTactileWarningStripKerb : OsmFilterQuestType<WDMTactileWarningStrip>() {

    override val elementFilter = "nodes with Obstacle and Obstacle = Kerb and !TactileWarningStrip"
    override val changesetComment = "Specify type of tactile warning strip kerb"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_tactile_warning_strip
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_tactile_warning_strip_kerb_title

    override fun createForm() = AddWDMTactileWarningStripKerbForm()

    override fun applyAnswerTo(answer: WDMTactileWarningStrip, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        when (answer) {
            NONE -> {
                tags["TactileWarningStrip"] = "None"
            }
            GOOD -> {
                tags["TactileWarningStrip"] = "Good"
            }
            WORN -> {
                tags["TactileWarningStrip"] = "Worn"
            }
            DISCOMFORTABLE -> {
                tags["TactileWarningStrip"] = "Discomfortable"
            }
            HAZARDOUS -> {
                tags["TactileWarningStrip"] = "Hazardous"
            }
        }
    }
}

package de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip

import de.westnordost.streetcomplete.quests.AImageListQuestForm

class AddWDMTactileWarningStripKerbForm : AImageListQuestForm<WDMTactileWarningStrip, WDMTactileWarningStrip>() {

    override val items = WDMTactileWarningStrip.values().map { it.asItem() }
    override val itemsPerRow = 3

    override fun onClickOk(selectedItems: List<WDMTactileWarningStrip>) {
        applyAnswer(selectedItems.single())
    }
}

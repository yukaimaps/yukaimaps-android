package de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.NONE
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.GOOD
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.WORN
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.DISCOMFORTABLE
import de.westnordost.streetcomplete.quests.wdm.tactile_warning_strip.WDMTactileWarningStrip.HAZARDOUS
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMTactileWarningStrip.asItem() = Item(this, iconResId, titleResId)

private val WDMTactileWarningStrip.titleResId: Int get() = when (this) {
    NONE ->  R.string.quest_wdm_tactile_warning_strip_none
    GOOD -> R.string.quest_wdm_tactile_warning_strip_good
    WORN ->      R.string.quest_wdm_tactile_warning_strip_worn
    DISCOMFORTABLE ->      R.string.quest_wdm_tactile_warning_strip_discomfortable
    HAZARDOUS ->      R.string.quest_wdm_tactile_warning_strip_hazardous
}

private val WDMTactileWarningStrip.iconResId: Int get() = when (this) {
    NONE ->  R.drawable.wdm_tactile_warning_strip_no
    GOOD -> R.drawable.wdm_tactile_warning_strip_good
    WORN ->      R.drawable.wdm_tactile_warning_strip_worn
    DISCOMFORTABLE ->      R.drawable.wdm_tactile_warning_strip_discomfortable
    HAZARDOUS ->      R.drawable.wdm_tactile_warning_strip_hazardous
}

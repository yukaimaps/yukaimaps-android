package de.westnordost.streetcomplete.quests.wdm.toilet

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.util.ktx.toYesNo

class AddWDMToilets : OsmElementQuestType<WDMToiletsAnswer> {

    private val filter by lazy { """
        nodes, ways with PointOfInterest = StopPlace
        and !Toilets
    """.toElementFilterExpression() }

    override val changesetComment = "Specify what kind of toilet"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_toilets
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_toilet_title

    override fun getApplicableElements(mapData: MapDataWithGeometry): Iterable<Element> =
        mapData.nodes.filter { isApplicableTo(it) }

    override fun isApplicableTo(element: Element): Boolean =
        /* Only recycling containers that do either not have any recycling:* tag yet or
         * haven't been touched for 2 years and are exclusively recycling types selectable in
         * StreetComplete. */
        filter.matches(element)

    override fun createForm() = AddWDMToiletsForm()

    override fun applyAnswerTo(answer: WDMToiletsAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        if (answer is WDMToilets) {
            applyWDMToiletAnswer(answer.toilets, tags)
        }
    }

    private fun applyWDMToiletAnswer(toilets: List<WDMToilet>, tags: Tags) {
        val selectedToilets = toilets.map { "${it.value}" }
        for (toilet in selectedToilets) {
            tags["Toilets"] = toilet;
        }
    }
}

package de.westnordost.streetcomplete.quests.wdm.toilet

/** All assistance:* keys known to StreetComplete */
enum class WDMToilet(val value: String) {
    PMR("Yes"),
    NOPMR("Bad"),
    NONE("No");

    companion object {
        val selectableValues = listOf(
            PMR, NOPMR, NONE
        )
    }
}

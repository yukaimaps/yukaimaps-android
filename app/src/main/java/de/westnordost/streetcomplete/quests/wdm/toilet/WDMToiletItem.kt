package de.westnordost.streetcomplete.quests.wdm.toilet

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.wdm.toilet.WDMToilet.PMR
import de.westnordost.streetcomplete.quests.wdm.toilet.WDMToilet.NOPMR
import de.westnordost.streetcomplete.quests.wdm.toilet.WDMToilet.NONE
import de.westnordost.streetcomplete.view.image_select.Item

fun WDMToilet.asItem(): Item<List<WDMToilet>> =
    Item(listOf(this), iconResId, titleResId)

private val WDMToilet.iconResId: Int get() = when (this) {
    PMR ->     R.drawable.ic_wdm_toilets_pmr //TODO
    NOPMR ->             R.drawable.ic_wdm_toilets_nopmr  //TODO
    NONE ->             R.drawable.ic_wdm_toilets_none  //TODO
}

private val WDMToilet.titleResId: Int get() = when (this) {
    PMR ->     R.string.quest_wdm_toilets_pmr
    NOPMR ->             R.string.quest_wdm_toilets_nopmr
    NONE ->             R.string.quest_wdm_toilets_none
}

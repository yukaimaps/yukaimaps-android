package de.westnordost.streetcomplete.quests.wdm.toilet

sealed interface WDMToiletsAnswer

data class WDMToilets(val toilets: List<WDMToilet>) : WDMToiletsAnswer

package de.westnordost.streetcomplete.quests.wdm.visual_signs

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMVisualSignsErp : OsmFilterQuestType<WDMVisualSigns>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest
        and PointOfInterest !~ StopPlace
        and !VisualSigns
    """
    override val changesetComment = "WDM Survey erp visual signs"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_visual_signs_erp
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_visual_signs_erp_title

    override fun createForm() = AddWDMVisualSignsErpForm()

    override fun applyAnswerTo(answer: WDMVisualSigns, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["VisualSigns"] = answer.osmValue
    }
}

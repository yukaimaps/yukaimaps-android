package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.osmquests.OsmFilterQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement.WHEELCHAIR
import de.westnordost.streetcomplete.osm.Tags

class AddWDMWheelchairAccessErpLimitedNote : OsmFilterQuestType<String>() {

    override val elementFilter = """
        nodes, ways with PointOfInterest
        and PointOfInterest !~ StopPlace
        and !WheelchairAccess:Description
    """
    override val changesetComment = "WDM Survey limited note wheelchair accessibility of erps"
    override val wikiLink = null
    override val icon = R.drawable.ic_wdm_wheelchair_shop
    override val achievements = listOf(WHEELCHAIR)

    override fun getTitle(tags: Map<String, String>) = R.string.quest_wdm_wheelchairAccess_erp_limited_note_title

    override fun createForm() = AddWDMWheelchairAccessErpLimitedNoteForm()

    override fun applyAnswerTo(answer: String, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        tags["WheelchairAccess:Description"] = answer
    }
}

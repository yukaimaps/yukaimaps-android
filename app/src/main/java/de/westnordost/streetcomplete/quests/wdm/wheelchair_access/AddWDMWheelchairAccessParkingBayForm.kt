package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R

class AddWDMWheelchairAccessParkingBayForm : WDMWheelchairAccessForm() {
    override val contentLayoutResId = R.layout.quest_wdm_wheelchair_parkingbay_explanation
}

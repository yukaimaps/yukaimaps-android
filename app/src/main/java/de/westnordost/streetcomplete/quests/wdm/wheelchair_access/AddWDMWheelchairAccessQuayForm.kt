package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R

class AddWDMWheelchairAccessQuayForm : WDMWheelchairAccessForm() {
    override val contentLayoutResId = R.layout.quest_wdm_wheelchair_quay_explanation
}

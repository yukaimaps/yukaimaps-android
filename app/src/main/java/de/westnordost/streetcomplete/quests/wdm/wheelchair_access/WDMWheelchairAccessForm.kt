package de.westnordost.streetcomplete.quests.wdm.wheelchair_access

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.quests.AbstractOsmQuestForm
import de.westnordost.streetcomplete.quests.AnswerItem
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.WDMWheelchairAccess.LIMITED
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.WDMWheelchairAccess.NO
import de.westnordost.streetcomplete.quests.wdm.wheelchair_access.WDMWheelchairAccess.YES

open class WDMWheelchairAccessForm : AbstractOsmQuestForm<WDMWheelchairAccess>() {

    override val buttonPanelAnswers = listOf(
        AnswerItem(R.string.quest_generic_hasFeature_no) { applyAnswer(NO) },
        AnswerItem(R.string.quest_generic_hasFeature_yes) { applyAnswer(YES) },
        AnswerItem(R.string.quest_wheelchairAccess_limited) {
            applyAnswer(LIMITED)
        },
    )
}

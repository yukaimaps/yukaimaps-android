package de.westnordost.streetcomplete.quests.wdm.width

import de.westnordost.streetcomplete.R
import de.westnordost.streetcomplete.data.elementfilter.toElementFilterExpression
import de.westnordost.streetcomplete.data.osm.geometry.ElementGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.Element
import de.westnordost.streetcomplete.data.osm.mapdata.MapDataWithGeometry
import de.westnordost.streetcomplete.data.osm.mapdata.filter
import de.westnordost.streetcomplete.data.osm.osmquests.OsmElementQuestType
import de.westnordost.streetcomplete.data.user.achievements.EditTypeAchievement
import de.westnordost.streetcomplete.osm.MAXSPEED_TYPE_KEYS
import de.westnordost.streetcomplete.osm.ROADS_ASSUMED_TO_BE_PAVED
import de.westnordost.streetcomplete.osm.Tags
import de.westnordost.streetcomplete.osm.surface.ANYTHING_PAVED
import de.westnordost.streetcomplete.screens.measure.ArSupportChecker

class AddWDMSitePathLinkWidth(
    private val checkArSupport: ArSupportChecker
) : OsmElementQuestType<WDMWidthAnswer> {

    private val wayFilter by lazy { """
        ways with SitePathLink
        and SitePathLink !~ Stairs|Escalator
        and !Width
    """.toElementFilterExpression() }

    override val changesetComment = "WDM Determine sitepathlink widths"
    override val wikiLink = null
    override val icon = R.drawable.ic_quest_wdm_street_width
    override val achievements = listOf(EditTypeAchievement.WHEELCHAIR)
    override val defaultDisabledMessage: Int
        get() = if (!checkArSupport()) R.string.default_disabled_msg_no_ar else 0

    override fun getTitle(tags: Map<String, String>) =
        if(tags["SitePathLink"] == "Pavement")
            R.string.quest_wdm_site_path_link_pavement_width_title
        else if(tags["SitePathLink"] == "Footpath")
            R.string.quest_wdm_site_path_link_footpath_width_title
        else if(tags["SitePathLink"] == "Street")
            R.string.quest_wdm_site_path_link_street_width_title
        else if(tags["SitePathLink"] == "OpenSpace")
            R.string.quest_wdm_site_path_link_openspace_width_title
        else if(tags["SitePathLink"] == "Concourse")
            R.string.quest_wdm_site_path_link_concourse_width_title
        else if(tags["SitePathLink"] == "Passage")
            R.string.quest_wdm_site_path_link_passage_width_title
        else if(tags["SitePathLink"] == "Ramp")
            R.string.quest_wdm_site_path_link_ramp_width_title
        else if(tags["SitePathLink"] == "Crossing")
            R.string.quest_wdm_site_path_link_crossing_width_title
        else if(tags["SitePathLink"] == "Quay")
            R.string.quest_wdm_site_path_link_quay_width_title
        else
            R.string.quest_wdm_site_path_link_width_title

    override fun getApplicableElements(mapData: MapDataWithGeometry) =
        mapData.ways.filter { wayFilter.matches(it) }

    override fun isApplicableTo(element: Element) =
        wayFilter.matches(element)

    override fun createForm() = AddWDMWidthForm()

    override fun applyAnswerTo(answer: WDMWidthAnswer, tags: Tags, geometry: ElementGeometry, timestampEdited: Long) {
        val key = "Width"

        tags[key] = answer.width.toOsmValue()

        if (answer.isARMeasurement) {
            tags["source:$key"] = "ARCore"
        } else {
            tags.remove("source:$key")
        }
    }
}

package de.westnordost.streetcomplete.quests.wdm.width

import de.westnordost.streetcomplete.osm.Length

data class WDMWidthAnswer(val width: Length, val isARMeasurement: Boolean)

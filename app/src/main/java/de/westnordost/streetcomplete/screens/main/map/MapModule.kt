package de.westnordost.streetcomplete.screens.main.map

import org.koin.dsl.module

val mapModule = module {
    factory<VectorTileProvider> {
        object : VectorTileProvider(
            "IGN",
            16,
            "© IGN",
            "https://geoservices.ign.fr",
            "https://geoservices.ign.fr/mentions-legales",
            "map_theme",
            "",
        ) {
            override fun getTileUrl(zoom: Int, x: Int, y: Int): String =
                "https://data.geopf.fr/tms/1.0.0/PLAN.IGN/$zoom/$x/$y.pbf"
        }
    }

    single { TangramPinsSpriteSheet(get(), get(), get(), get()) }
    single { TangramIconsSpriteSheet(get(), get()) }
}

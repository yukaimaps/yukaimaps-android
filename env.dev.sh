export ANDROID_APP_NAME="Acceslibre Mobilités Dev"
export ANDROID_APP_ID="fr.acceslibremobilites.dev"

export WEB_ROOT=https://dev.yukaimaps.someware.fr

export OAUTH_CLIENT_ID=dev.yukaimaps.someware.fr

export OIDC_INSECURE=false
